cmake_minimum_required(VERSION 3.0)

project(good
  LANGUAGES C)

add_subdirectory(src)
add_subdirectory(playground)
add_subdirectory(thirdparty/Catch2)

option(GOOD_BUILD_TESTING "Build the unit testing project for the 'good' library." ON)
if (GOOD_BUILD_TESTING)
  add_subdirectory(tests)
endif()
