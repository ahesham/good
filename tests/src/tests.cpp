#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include <stdbool.h>

#define GOOD_ARRAY_IMPLEMENTATION
#include <good/dynamic_array.h>

TEST_CASE("Check if array is empty", "[good][array][length][capacity]")
{
    int *A = NULL;

    SECTION("Checking an uninitialized array.")
    {
        REQUIRE(array_empty(A) == true);
    }

    SECTION("Adding an element to the array.")
    {
        array_push(A, 5);
        REQUIRE(array_empty(A) == false);
    }

    array_free(A);
}
