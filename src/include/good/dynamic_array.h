#pragma once

#define min(a, b) (((a) < (b)) ? (a) : (b))

/******************************************************************************
 * Types
 *****************************************************************************/

typedef struct
{
    int length;
    int capacity;
} gd__array;

/******************************************************************************
 * Pretty interface
 *****************************************************************************/

#define array_empty   gd_arr_empty
#define array_last    gd_arr_last
#define array_add     gd_arr_add
#define array_push    gd_arr_push
#define array_pop     gd_arr_pop
#define array_length  gd_arr_length
#define array_insert  gd_arr_insert
#define array_delete  gd_arr_delete
#define array_free    gd_arr_free

/******************************************************************************
 * Length
 *****************************************************************************/

// Access the header information stored
// _before_ the actual array data
#define gd_arr_head(A)   (((gd__array*)(A)) - 1)

// Check if array is empty
#define gd_arr_empty(A)    (gd_arr_length(A) == 0)

// Add 1 uninitialized element to the array
#define gd_arr_add(A)      (gd_arr_addn((A), 1))

// Add 'n' uninitialized elements to the array
#define gd_arr_addn(A, n)  (gd__arr_addn((A), n), (A) + gd_arr_length(A) - (n))

// Change the allocated length of the array
#define gd__arr_grow(A, n) (gd_arr_head(A)->length += (n))

// Add 'n' uninitialized elements to the array and return
// a pointer to the first new element
#define gd__arr_addn(A, n) (gd_arr_length(A) + (n) > gd_arr_capacity(A) ?          \
                           (gd__arr_add_length((void**)&(A), sizeof(*A), (n)), 0) : \
                           ((gd__arr_grow(A, n), 0)))

// Append 1 element to the end of the array
#define gd_arr_push(A, e)  (*gd_arr_add(A) = (e))

// Get the length of the array (0 if NULL)
#define gd_arr_length(A)   (A ? gd_arr_head(A)->length : 0)

// Get the last element in the array
#define gd_arr_last(A)     ((A)[gd_arr_length(A) - 1])

// Remove the last element in the array and return it
#define gd_arr_pop(A)      ((A)[--gd_arr_head(A)->length])

// Insert 'n' uninitialized elements starting from the 'i'th element (array index starts at 0)
#define gd_arr_insertn(A, i, n)  (gd__arr_insertn((void**)&(A), sizeof(*A), i, n))

// Insert an uninitialized element at the 'i'th index
// and assign it the value 'e' (array index starts at 0)
#define gd_arr_insert(A, i, e)   (gd__arr_insertn((void**)&(A), sizeof(*A), i, 1), ((A)[i] = e))

// Delete 'n' elements starting from the 'i'th element (array index starts at 0)
#define gd_arr_deleten(A, i, n)  (gd__arr_deleten((void**)&(A), sizeof(*A), i, n))

// Delete the 'i'th element (array index starts at 0)
#define gd_arr_delete(A, i)  gd_arr_deleten(A, i, 1)

// Delete the 'i'th element, swap down from the end of the array
// TODO
// #define gd_arr_fast_delete(A, i)

/******************************************************************************
 * Storage
 *****************************************************************************/

// Get the array capacity (0 if NULL)
#define gd_arr_capacity(A)    (A ? gd_arr_head(A)->capacity : 0)

// Set the capacity of the array to 'n' (we are expecting to grow soon)
#define gd_arr_set_size(A, n)  (/*TODO*/)

// Check that the array has enough capacity for incoming 'n' new allocations
#define gd_arr_can(A, n)      (gd_arr_length(A) + (n) > gd_arr_capacity(A) ? \
                              gd_arr_set_size((A), (n)) : 0)

// Make a copy of the contents of the array (copy using memcpy)
#define gd_arr_copy(A)        gd__arr_copy(A, sizeof((A)[0]))

// Calculate the storage needed to store the array
#define gd_arr_storage(A)     (gd_arr_length(A) * sizeof((A)[0]))

/******************************************************************************
 * Implementation
 *****************************************************************************/

extern void  _gd_arr_free(void **);
extern void* _gd__arr_copy(void *, int);
extern void  _gd__arr_set_size(void **, int, int);
extern void  _gd__arr_set_length(void **, int, int);
extern void  _gd__arr_add_length(void **, int, int);
extern void  _gd__arr_deleten(void **, int, int, int);
extern void  _gd__arr_insertn(void **, int, int, int);

#define gd_arr_free(A)      _gd_arr_free((void**)&(A))
#define gd__arr_copy        _gd__arr_copy
#define gd__arr_set_size    _gd__arr_set_size
#define gd__arr_set_length  _gd__arr_set_length
#define gd__arr_add_length  _gd__arr_add_length
#define gd__arr_deleten     _gd__arr_deleten
#define gd__arr_insertn     _gd__arr_insertn

#if defined (GOOD_ARRAY_IMPLEMENTATION)

#include <stdlib.h>
#include <string.h>

static void *gd__arr_state;

void
*gd_arr_malloc_parent(void *p)
{
    void *q = gd__arr_state;
    gd__arr_state = p;
    return q;
}

//void
//gd_arr_malloc(void **pointer_to_pointer, void *state)
//{
//    // TODO: ALLOCATE OURSELVES YE COW!!!
//    gd__array *array = malloc(sizeof(*array));
//    array->length = array->capacity = 0;
//    array->own = true;
//    *pointer_to_pointer = (void*)(array + 1);
//}

void
_gd_arr_free(void **pointer_to_pointer)
{
    void *pointer = *pointer_to_pointer;
    if (pointer)
    {
        gd__array *array = gd_arr_head(pointer);
        // TODO: NEED TO KNOW IF WE ARE THE OWNERS OF THE ARRAY (THROUGH COPY)
        free(array);
    }

    *pointer_to_pointer = NULL;
}

void*
_gd__arr_copy(void *pointer, int element_size)
{
    if (pointer == NULL) return NULL;

    gd__array *array;
    // WE NEED TO KNOW IF WE ARE THE OWNERS OF THE NEW ARRAY
    // TODO
}

static void
_gd__arr_size(void **pointer_to_pointer, int size, int capacity, int length)
{
    void *pointer = *pointer_to_pointer;
    gd__array *array;

    if (pointer == NULL)
    {
        if (length == 0 && size == 0) return;

        array = (gd__array*)malloc(sizeof(*array) + size * capacity);
        array->length = length;
        array->capacity = capacity;
    }
    else
    {
        array = gd_arr_head(pointer);
        array->length = length;

        // If the required capacity exceeds our current capacity
        // we need to expand!
        if (array->capacity < capacity)
        {
            void *pointer;

            // Sane (?) expansion
            // TODO: Check sanity (x d)
            if (array->capacity >= 4 &&
                capacity < array->capacity * 2)
            {
                capacity = array->capacity * 2;
            }

            pointer = realloc(array, sizeof(*array) + size * capacity);
            if (pointer != NULL)
            {
                array = (gd__array*)pointer;
                array->capacity = capacity;
            }
            else
            {
                // TODO: error (system out of memory?)
            }
        }
    }

    // TODO: write our own min
    array->length = min(array->length, array->capacity);
    *pointer_to_pointer = array + 1;
}

void
_gd__arr_set_size(void **pointer_to_pointer, int size, int capacity)
{
    void *pointer = *pointer_to_pointer;
    _gd__arr_size(pointer_to_pointer, size, capacity, gd_arr_length(pointer));
}

void
_gd__arr_set_length(void **pointer_to_pointer, int size, int new_length)
{
    void *pointer = *pointer_to_pointer;
    if (gd_arr_capacity(pointer) < new_length || pointer == NULL)
    {
        _gd__arr_size(pointer_to_pointer, size, new_length, new_length);
    }
    else
    {
        gd_arr_head(pointer)->length = new_length;
    }
}

void
_gd__arr_add_length(void **pointer_to_pointer, int size, int additional_length)
{
    _gd__arr_set_length(pointer_to_pointer, size, gd_arr_length(*pointer_to_pointer) + additional_length);
}

void
_gd__arr_deleten(void **pointer_to_pointer, int size, int index, int count)
{
    void *pointer = *pointer_to_pointer;

    if (count)
    {
        memmove((char*)pointer + index * size,
                (char*)pointer + (index + count) * size,
                size * (gd_arr_length(pointer) - (index + count)));
        gd_arr_head(pointer)->length -= count;
    }

    *pointer_to_pointer = pointer;
}

void
_gd__arr_insertn(void **pointer_to_pointer, int size, int index, int count)
{
    void *pointer = *pointer_to_pointer;

    if (count)
    {
        if (pointer == NULL)
        {
            _gd__arr_add_length(pointer_to_pointer, size, count);
            return;
        }

        int length_before_expansion = gd_arr_length(pointer);
        _gd__arr_add_length(&pointer, size, count);
        memmove((char*)pointer + (index + count) * size,
                (char*)pointer + index * size,
                size * (length_before_expansion - index));
    }

    *pointer_to_pointer = pointer;
}

#endif
