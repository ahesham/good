#include <stdio.h>

#define GOOD_ARRAY_IMPLEMENTATION
#include <good/dynamic_array.h>

int main(int argc, char* argv[])
{
    printf("Hello, World!\n\n");

    // Create an array
    int *array = NULL;

    // Push elements onto the array (dynamically, w00p)
    array_push(array, 0);
    array_push(array, 42);
    array_push(array, -1);
    array_push(array, 55);

    // Print elements in the array
    for (int i = 0; i < array_length(array); ++i)
    {
        printf("The value in array[%d] = %d\n", i, array[i]);
    }

    // Insert an element at the front
    array_insert(array, 4, 7);

    // Print elements in the array
    for (int i = 0; i < array_length(array); ++i)
    {
        printf("\nThe value in array[%d] = %d\n", i, array[i]);
    }

    // Delete an element and check size
    printf("\nDeletion squad: size of array now is: %d\n", array_length(array));
    array_delete(array, 0);
    // Print elements in the array
    for (int i = 0; i < array_length(array); ++i)
    {
        printf("The value in array[%d] = %d\n", i, array[i]);
    }

    printf("Deletion squad: size of array now is: %d\n", array_length(array));

    // Clean bois
    array_free(array);
    array = NULL;

    printf("\nSkeptical bois: size of array is: %d", array_length(array));

    return 0;
}
